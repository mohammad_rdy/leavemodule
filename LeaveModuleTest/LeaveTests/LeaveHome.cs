﻿using LeaveModule;
using LeaveModule.Leave;
using LeaveModule.Login;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveModuleTest.LeaveTests
{
    public class LeaveHome :  BaseClass
    {
        private string Username = "mohammadarafat@epayroll.com";
        private string Password = "abcde12345";
        private string LeaveStartDate = DateTime.Now.ToString("dd/MM/yyyy");
        private string LeaveEndDate = DateTime.Now.AddDays(3).ToString("dd/MM/yyyy");


        /*   -------------- Test Cases Covered in the following Test -------------------- 
         *   
         * An Authority can apply leave on beahlf of an employee 
         * The Leave dates are appearing correctly in the leave application dashboard 
         * Leave can be cancelled from the leave application dashboard
         
         ----------------------------------X----------------------------------------------*/

        [Test]
        [Obsolete]
        public void CanApplyForALeaveAsAnApprover()
        {
            // Act
            Login._Login(Username, Password);
            LeaveApplicationHome.NavigateToLeaveHome();
            LeaveApplicationHome.NavigateToLeaveApplicationForm();
            LeaveApplicationHome.InsertLeaveDates(LeaveStartDate, LeaveEndDate);
            LeaveApplicationHome.SubmitLeave();

            // Assert
            Assert.AreEqual(LeaveValidation.Last_Day_at_Work, LeaveStartDate);
            Assert.AreEqual(LeaveValidation.FirstDayBackAtWork, LeaveEndDate);

            //CleanUpData
            LeaveApplicationHome.CancelLeave();

        }

        /*   -------------- Test Cases Covered in the following Test -------------------- 
        *   
        * An Authority can apply leave on beahlf of an employee 
        * The Leave dates are appearing correctly in the leave application dashboard 
        * Leave can be approved from the leave application dashboard

        ----------------------------------X----------------------------------------------*/

        [Test]
        [Obsolete]
        public void CanApproveALeaveAsAnApprover()
        {
            // Act
            Login._Login(Username, Password);
            LeaveApplicationHome.NavigateToLeaveHome();
            LeaveApplicationHome.NavigateToLeaveApplicationForm();
            LeaveApplicationHome.InsertLeaveDates(LeaveStartDate, LeaveEndDate);
            LeaveApplicationHome.SubmitLeave();
            LeaveApplicationHome.ApproveLeave();

            // Assert
            Assert.AreEqual(LeaveValidation.Last_Day_at_Work, LeaveStartDate);
            Assert.AreEqual(LeaveValidation.FirstDayBackAtWork, LeaveEndDate);
            Assert.IsTrue(LeaveValidation.LeaveApprovalStatus.Contains("Accepted");

            //CleanUpData
            LeaveApplicationHome.CancelLeave();

        }

    }
}
