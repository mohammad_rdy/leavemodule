﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveModule
{
    public class BaseClass
    {

        [OneTimeSetUp]
        public void Initialise()
        {

            Driver.Initialize();
        }

        [TearDown]
        public void TestResults()
        {

            Driver.Instance.Manage().Cookies.DeleteAllCookies();
        }

        [OneTimeTearDown]
        public void GetResult()
        {

            Driver.Close();
            //MailNotification.SendMail(TestName, Result);
        }

    }
}
