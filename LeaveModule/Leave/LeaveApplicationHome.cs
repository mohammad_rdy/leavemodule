﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveModule.Leave
{
   public class LeaveApplicationHome 
    {
        [Obsolete]
        public static void NavigateToLeaveHome()

        {
            var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(40));
            try
            {
                Driver.Instance.FindElement(By.XPath(@"//a[contains(text(),'Leave / Expense')]")).Click();
                Driver.Instance.FindElement(By.XPath(@"//a[contains(@href,'/lema/default.aspx')]")).Click();
                wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath("//button[@id='phMain_ctl02_butLeaveApply']")));
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
            }
        }


        [Obsolete]
        public static void NavigateToLeaveApplicationForm()
        {
            var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(40));
            try
            {
                System.Threading.Thread.Sleep(2000);
                SelectElement _person = new SelectElement(Driver.Instance.FindElement(By.Id("phMain_ctl02_ddlLeaveAuthorities")));
                _person.SelectByText("ABBINGTON, Anastasia (Employees)");
                Driver.Instance.FindElement(By.XPath(@"//button[contains(text(),'Apply for Leave')]")).Click();
                wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath("//span[@id='phMain_lblEmployeeName']")));
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
            }
        }

        public static void InsertLeaveDates(string startdate, string enddate)
        {
            try
            {
                Driver.Instance.FindElement(By.XPath(@"//input[@id='ctl00_phMain_txtFrom_dateInput']")).Clear();
                System.Threading.Thread.Sleep(2000);
                Driver.Instance.FindElement(By.XPath(@"//input[@id='ctl00_phMain_txtFrom_dateInput']")).SendKeys(startdate);
                System.Threading.Thread.Sleep(2000);
                Driver.Instance.FindElement(By.XPath(@"//input[@id='ctl00_phMain_txtTo_dateInput']")).Clear();
                System.Threading.Thread.Sleep(2000);
                Driver.Instance.FindElement(By.XPath(@"//input[@id='ctl00_phMain_txtTo_dateInput']")).SendKeys(enddate);
                System.Threading.Thread.Sleep(2000);
                Driver.Instance.FindElement(By.XPath(@"//textarea[@id='phMain_txtComments']")).Click();
                System.Threading.Thread.Sleep(2000);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
            }
        }

        [Obsolete]
        public static void SubmitLeave()
        {
            var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(40));
            try
            {
                Driver.Instance.FindElement(By.XPath(@"//a[@id='phMain_butSubmit']")).Click();
                wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath("//table[@id='phMain_ctl02_dgLeave']//a[contains(text(),'Employee')]")));

                System.Threading.Thread.Sleep(2000);
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
            }
        }

        [Obsolete]
        public static void CancelLeave()
        {
            var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(40));
            try
            {
                Driver.Instance.FindElement(By.XPath(@"//a[@id='phMain_ctl02_dgLeave_EditLink_0']")).Click();
                wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath("//a[@id='phMain_butCancelTop']")));
                Driver.Instance.FindElement(By.XPath(@"//a[@id='phMain_butCancelTop']")).Click();
                wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath("//*[@id='phMain_ctl02_butLeaveApply']")));
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
            }
            
        }

        [Obsolete]
        public static void ApproveLeave()
        {
            var wait = new WebDriverWait(Driver.Instance, TimeSpan.FromSeconds(40));
            try
            {
                Driver.Instance.FindElement(By.XPath(@"//a[@id='phMain_ctl02_dgLeave_EditLink_0']")).Click();
                wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath("//*[@id='phMain_butAcceptTop']")));
                Driver.Instance.FindElement(By.XPath(@"//*[@id='phMain_butAcceptTop']")).Click();
                wait.Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath("//*[@id='phMain_ctl02_butLeaveApply']")));
            }
            catch (Exception e)
            {
                Console.Write(e.Message.ToString());
            }
            
        }


    }
}
