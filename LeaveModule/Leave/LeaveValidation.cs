﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveModule.Leave
{
    public class LeaveValidation
    {
        public static string Last_Day_at_Work
        { 
         get
            {
                var value = Driver.Instance.FindElement(By.XPath(@"//*[@id='phMain_ctl02_dgLeave']/tbody/tr[2]/td[4]/span"));
                return value.Text;
            }
        }

        public static string FirstDayBackAtWork
        { 
            get
            {
                var value = Driver.Instance.FindElement(By.XPath(@"//*[@id='phMain_ctl02_dgLeave']/tbody/tr[2]/td[5]/span"));
                return value.Text;
            }
        }

        public static string LeaveApprovalStatus
        {
            get
            {
                var value = Driver.Instance.FindElement(By.XPath(@"//*[@id='phMain_ctl02_dgLeave']/tbody/tr[2]/td[9]"));
                return value.Text;
            }
        }
    }
}
